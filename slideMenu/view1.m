//
//  view1.m
//  slideMenu
//
//  Created by Cli16 on 11/18/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "view1.h"
UITableView *showTableView;
NSMutableArray *colorsNames;
@implementation view1

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    colorsNames=[[NSMutableArray alloc]initWithObjects:@"Red",@"Green",@"White",@"black",@"yellow",@"pink",@"blue",@"gray",@"orange",@"maganta",@"sky blue", nil];
    
    
    showTableView=[[UITableView alloc]init];
    showTableView.frame=CGRectMake(0, 100, 230, 350);
    showTableView.backgroundColor=[UIColor clearColor];
    [showTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell1"];
    
   showTableView.delegate=self;
    showTableView.dataSource=self;
    [self addSubview:showTableView];
    [showTableView reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return colorsNames.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier=@"cell1";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    cell.textLabel.text=colorsNames[indexPath.row];

    return cell;
    
}
@end
