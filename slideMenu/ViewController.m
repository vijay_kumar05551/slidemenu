//
//  ViewController.m
//  slideMenu
//
//  Created by Cli16 on 11/18/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *sliderView;
@property (strong, nonatomic) IBOutlet UIButton *pressBtn;

@end

@implementation ViewController
@synthesize sliderView;
@synthesize pressBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    sliderView.hidden=YES;
    
   }
- (IBAction)pressBtnClicked:(id)sender {
 sliderView .hidden=NO;
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
